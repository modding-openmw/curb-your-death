## Curb Your Death Changelog

#### 1.0

Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/curb-your-death/-/packages/15875249) | [Nexus](https://www.nexusmods.com/morrowind/mods/53168)
