# Curb Your Death

Play "Frolic" by Luciano Michelini, also known as the theme song from the show Curb Your Enthusiasm, when your character dies.

## Credits

**Packaged by**: johnnyhostile

**Special Thanks**:

* Luciano Michelini for writing "Frolic"
* Larry David for making [Curb Your Enthusiasm](https://en.wikipedia.org/wiki/Curb_Your_Enthusiasm)
* Vandolph09 for making [Seinfeld Credits Death Song](https://www.nexusmods.com/newvegas/mods/64877) (which inspired this)
